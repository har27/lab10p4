/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab10p4;

/**
 *
 * @author hptop
 */
public class CreditPaymentService extends PaymentService {
	
	//No implementation mentioned, so I've just put a simple print statement
	public void processPayment(double amount) {
		System.out.println("Process payment CreditPaymentService");
	}
}