/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab10p4;

/**
 *
 * @author hptop
 */
import java.util.ArrayList;
import java.util.List;

public class Cart {

	//instance variables
	private List<Product> products = new ArrayList<>();
	private PaymentService service;
	
	public void setPaymentService(PaymentService service) {
		this.service = service;
	}
	
	public void addProduct(Product product) {
		this.products.add(product);
	}
	
	//No implementation mentioned, so I've just put a simple print statement
	public void payCart() {
		System.out.println("Pay cart");
	}

	//getter and setter methods
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public PaymentService getService() {
		return service;
	}

	public void setService(PaymentService service) {
		this.service = service;
	}
}
 
