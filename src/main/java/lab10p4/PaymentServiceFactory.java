/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab10p4;

/**
 *
 * @author hptop
 */
public class PaymentServiceFactory {

	private static PaymentServiceFactory instance = null;
	
	private PaymentServiceFactory(){}
	
	public static PaymentServiceFactory getInstance() {
		if (instance == null)
			instance = new PaymentServiceFactory();

		return instance;
	}
	
	public PaymentService getPaymentServiceType(PaymentServiceType type) {
		if(type == null) 
			return null;
		
		if(type == PaymentServiceType.CREDIT) {
			return new CreditPaymentService();
		} else if (type == PaymentServiceType.DEBIT) {
			return new DebitPaymentService();
		}
		
		return null;
	}
}
