/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab10p4;

/**
 *
 * @author hptop
 */
public enum PaymentServiceType {
	CREDIT,
	DEBIT
}
